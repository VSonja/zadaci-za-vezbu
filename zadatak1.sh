#!/bin/bash

declare -a g1s1=("Ekonomija" "Matematika1" "Menadzment" "OIKT" "Psihologija" "Engleski1");
declare -a g1s2=("Matematika2" "Programiranje1" "UIS" "OO" "Proizvodni_sistemi");
declare -a g2s1=("AROS" "Matematika3" "Verovatnoca" "Marketing" "Programiranje2" "Engleski2");
declare -a g2s2=("DMS" "FMIR" "MTR" "SPA" "Statistika");
declare -a g3s1=("EPOS" "MLJR" "OI1" "RMT" "TS");
declare -a g3s2=("Baze" "LINS" "MPP" "OI2" "POIS" "PJ");
declare -a g4s1=("PIS" "ITEH" "SISJ" "PS" "Inteligentni");
declare -a g4s2=("OK" "Mobilno_racunarstvo" "ZRS" "NJT" "Diplomski");



kreiraj_foldere(){
	
	mkdir ${1}.godina;	
	cd ${1}.godina;	
	mkdir S1 S2;	
	
	declare s1;
	declare s2;
	
	case $1 in
		"1")
			s1=( "${g1s1[@]}" );
			s2=( "${g1s2[@]}" );
		;;
		"2")
			s1=( "${g2s1[@]}" );
			s2=( "${g2s2[@]}" );
		;;
		"3")
			s1=( "${g3s1[@]}" );
			s2=( "${g3s2[@]}" );
		;;	
		"4")
			s1=( "${g4s1[@]}" );
			s2=( "${g4s2[@]}" );
		;;
	esac
	
	cd S1;
	length=${#s1[@]};
	for (( i=0; i<${length}; i++ )); 
		do
			mkdir ${s1[$i]};
		done
		
	cd ..;
	cd S2;
	length=${#s2[@]};
	for (( i=0; i<${length}; i++ )); 
		do
			mkdir ${s2[$i]};
		done
		
	cd ../..
}

case $1 in
	"1")
		kreiraj_foldere 1;
	;;
	"2")
		kreiraj_foldere 2;
	;;
	"3")
		kreiraj_foldere 3;
	;;
	"4")
		kreiraj_foldere 4;
	;;
	"--all")
		kreiraj_foldere 1;
		kreiraj_foldere 2;
		kreiraj_foldere 3;
		kreiraj_foldere 4;
	;;

esac
